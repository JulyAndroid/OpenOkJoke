package com.xiaolei.openokjoke.Net


import com.xiaolei.easyfreamwork.application.ApplicationBreage
import com.xiaolei.easyfreamwork.utils.Log
import com.xiaolei.exretrofitcallback.network.Config
import com.xiaolei.okhttputil.Cookie.CookieJar
import com.xiaolei.okhttputil.OkHttpUtilConfig
import com.xiaolei.okhttputil.interceptor.CacheInterceptor
import com.xiaolei.openokjoke.BuildConfig
import com.xiaolei.openokjoke.Base.ResBodyBean
import com.xiaolei.openokjoke.Configs.Globals
import com.xiaolei.openokjoke.JNI.JokeLib
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


/**
 * Created by xiaolei on 2017/5/10.
 */

object BaseNetCore
{
    private val application by lazy {
        ApplicationBreage.getInstance().applicationContext
    }
    val cookieJar by lazy {
        CookieJar(application)
    }
    private val loggingInterceptor by lazy {
        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Log.d("HttpRetrofit", message + "")
        }
        ).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    private val okHttpClient by lazy {
        OkHttpClient.Builder()
                .addInterceptor(CacheInterceptor(ApplicationBreage.getInstance().context, CacheInterceptor.Type.SQLITE))
                .addInterceptor(loggingInterceptor)
                .retryOnConnectionFailure(true) //失败重连
                .connectTimeout(15, TimeUnit.SECONDS) //网络请求超时时间单位为秒
                .writeTimeout(10, TimeUnit.SECONDS) //写超时
                .readTimeout(10, TimeUnit.SECONDS) //读超时
                .cookieJar(cookieJar)
                .build()
    }
    private val retrofit by lazy {
        Retrofit.Builder()  //01:获取Retrofit对象
                .baseUrl(JokeLib.getServerAddress()) //02采用链式结构绑定Base url
                .addConverterFactory(ScalarsConverterFactory.create()) //首先判断是否需要转换成字符串，简单类型
                .addConverterFactory(GsonConverterFactory.create()) //再将转换成bean
                .client(okHttpClient)
                .build() //03执行操作
    }

    init
    {
        OkHttpUtilConfig.DEBUG = BuildConfig.DEBUG
        Config.registResponseBean(ResBodyBean::class.java, ResBodyRegister::class.java)
        Config.fiedFailEventClass = OnFaileEvent::class.java
    }

    fun <T> create(klass: Class<T>): T = retrofit.create(klass)

}
