package com.xiaolei.openokjoke.Net

import com.xiaolei.okhttputil.Catch.CacheHeaders
import com.xiaolei.openokjoke.Beans.JokeBean
import com.xiaolei.openokjoke.Beans.QQLuckyBean
import com.xiaolei.openokjoke.Base.ResBodyBean
import com.xiaolei.openokjoke.Beans.ShareMsgBean
import retrofit2.Call
import retrofit2.http.*


/**
 * 网络所有的API
 * Created by xiaolei on 2017/12/7.
 */
interface APPNet
{
    /**
     * 以普通模式按照分类获取笑话
     */
    @Headers(CacheHeaders.NORMAL)
    @GET(UriContext.joke)
    fun joke(@Query("type") type: JokeBean.Type): Call<ResBodyBean<List<JokeBean>>>

    /**
     * 以缓存优先的按照分类获取笑话
     */
    @Headers(CacheHeaders.CACHE_FIRST)
    @GET(UriContext.joke)
    fun cacheJoke(@Query("type") type: JokeBean.Type): Call<ResBodyBean<List<JokeBean>>>

    /**
     * 以普通的模式获取随机笑话
     */
    @Headers(CacheHeaders.NORMAL)
    @GET(UriContext.joke)
    fun randomJoke(): Call<ResBodyBean<List<JokeBean>>>

    /**
     * 以缓存优先的获取随机笑话
     */
    @Headers(CacheHeaders.CACHE_FIRST)
    @GET(UriContext.joke)
    fun cacheRandomJoke(): Call<ResBodyBean<List<JokeBean>>>

    /**
     * 反馈信息
     */
    @FormUrlEncoded
    @POST(UriContext.feedback)
    fun feedBack(@Field("context") context: String,
                 @Field("contact") contact: String?): Call<ResBodyBean<String>>

    /**
     * QQ测运
     */
    @Headers(CacheHeaders.CACHE_FIRST)
    @GET(UriContext.qqlucky)
    fun qqLucky(@Query("qq") qq: String): Call<ResBodyBean<QQLuckyBean>>

    /**
     * 分享反馈
     * @param phone_brand       手机品牌
     * @param phone_model       手机型号
     * @param android_version   安卓版本
     * @param phone_imei        IMEI号码
     * @param channel           来源渠道
     */
    @FormUrlEncoded
    @POST(UriContext.sharereport)
    fun sharedReport(@Field("phone_brand") phone_brand: String?,
                     @Field("phone_model") phone_model: String?,
                     @Field("android_version") android_version: String?,
                     @Field("phone_imei") phone_imei: String?,
                     @Field("channel") channel: String?): Call<ResBodyBean<String>>

    /**
     * 获取分享信息
     */
    @Headers(CacheHeaders.NORMAL)
    @GET(UriContext.sharemsg)
    fun sharemsg(@Query("channel") channel: String?): Call<ResBodyBean<ShareMsgBean>>

    /**
     * 不喜欢，举报，减少推送
     */
    @FormUrlEncoded
    @POST(UriContext.report)
    fun report(@Field("id") jokeid: String, @Field("imei") imei: String?): Call<ResBodyBean<String>>

    /**
     * 获取关于我们
     */
    @GET(UriContext.aboutme)
    fun aboutme():Call<ResBodyBean<String>>
    
}