package com.xiaolei.openokjoke.UM;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMEmoji;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMVideo;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.media.UMusic;
import com.xiaolei.easyfreamwork.utils.Log;
import com.xiaolei.exretrofitcallback.network.common.SCallBack;
import com.xiaolei.openokjoke.Exts.ExtensionsKt;
import com.xiaolei.openokjoke.Net.APPNet;
import com.xiaolei.openokjoke.Net.BaseRetrofit;
import com.xiaolei.openokjoke.Base.ResBodyBean;

import java.lang.ref.SoftReference;

import retrofit2.Call;


/**
 * 友盟分享的控件的工具
 * Created by xiaolei on 2017/3/13.
 */

public class UMShare
{
    private UMShareListener mShareListener;
    private SoftReference<Activity> mActivity;
    private ShareAction shareAction;
    private APPNet appNet = null;

    public UMShare(@Nullable final Activity activity)
    {
        mActivity = new SoftReference<>(activity);
        appNet = BaseRetrofit.INSTANCE.create(APPNet.class);
        mShareListener = new CustomShareListener(mActivity.get())
        {
            @Override
            public void onSuccess()
            {
                Log.e("UMShare", "分享成功");
                String phoneBrand = ExtensionsKt.getPhoneBrand(mActivity.get());
                String phoneModel = ExtensionsKt.getPhoneModel(mActivity.get());
                String androidVersion = ExtensionsKt.getAndroidVersion(mActivity.get());
                String imei = ExtensionsKt.getImei(mActivity.get());
                String channel = "";
                try
                {
                    ApplicationInfo appInfo = mActivity.get().getPackageManager().getApplicationInfo(mActivity.get().getPackageName(), PackageManager.GET_META_DATA);
                    channel = appInfo.metaData.getString("UMENG_CHANNEL");
                } catch (PackageManager.NameNotFoundException e)
                {
                    channel = "";
                }
                Call<ResBodyBean<String>> call = appNet.sharedReport(phoneBrand, phoneModel, androidVersion, imei, channel);
                call.enqueue(new SCallBack<ResBodyBean<String>>(activity)
                {
                    @Override
                    public void onSuccess(ResBodyBean<String> result) throws Exception
                    {
                    }

                    @Override
                    public void onFail(Throwable t)
                    {
                    }
                });
            }
        };
        shareAction = new ShareAction(mActivity.get());
        shareAction.setDisplayList(SHARE_MEDIA.WEIXIN
                , SHARE_MEDIA.WEIXIN_CIRCLE
                , SHARE_MEDIA.QQ
                , SHARE_MEDIA.QZONE
                , SHARE_MEDIA.SMS
                , SHARE_MEDIA.EMAIL)
                .setCallback(mShareListener);
    }

    /**
     * 纯文本分享
     *
     * @param text
     * @return
     */
    public UMShare shareText(String text)
    {
        shareAction.withText(text);
        return this;
    }

    /**
     * 图片分享<br/>
     * UMImage image = new UMImage(ShareActivity.this, "imageurl");//网络图片 <br/>
     * UMImage image = new UMImage(ShareActivity.this, file);//本地文件 <br/>
     * UMImage image = new UMImage(ShareActivity.this, R.drawable.xxx);//资源文件 <br/>
     * UMImage image = new UMImage(ShareActivity.this, bitmap);//bitmap文件 <br/>
     * UMImage image = new UMImage(ShareActivity.this, byte[]);//字节流 <br/>
     * 
     * 推荐使用网络图片和资源文件的方式，平台兼容性更高。<br/>
     * 对于部分平台，分享的图片需要设置缩略图，缩略图的设置规则为：<br/>
     * 
     * UMImage thumb =  new UMImage(this, R.drawable.thumb);<br/>
     * image.setThumb(thumb);<br/>
     * 
     * 用户设置的图片大小最好不要超过250k，缩略图不要超过18k， <br/>
     * 如果超过太多（最好不要分享1M以上的图片，压缩效率会很低），图片会被压缩。 <br/>
     * 用户可以设置压缩的方式： <br/>
     * 
     * //大小压缩，默认为大小压缩，适合普通很大的图<br/>
     * image.compressStyle = UMImage.CompressStyle.SCALE;<br/>
     * 
     * //质量压缩，适合长图的分享<br/>
     * image.compressStyle = UMImage.CompressStyle.QUALITY;<br/>
     *
     * 压缩格式设置<br/>
     * //用户分享透明背景的图片可以设置这种方式，但是qq好友，微信朋友圈，不支持透明背景图片，会变成黑色<br/>
     * image.compressFormat = Bitmap.CompressFormat.PNG;<br/>
     * 
     * @param image
     * @return
     */
    public UMShare shareImage(UMImage image)
    {
        shareAction.withMedia(image);
        return this;
    }

    /**
     * 分享链接
     * UMWeb  web = new UMWeb(Defaultcontent.url); <br/>
     * web.setTitle("This is music title");//标题 <br/>
     * web.setThumb(thumb);  //缩略图 <br/>
     * web.setDescription("my description");//描述 <br/>
     *
     * @param web
     * @return
     */
    public UMShare shareURL(UMWeb web)
    {
        shareAction.withMedia(web);
        return this;
    }

    /**
     * 分享视频<br/>
     * 视频只能使用网络视频
     * <p>
     * UMWeb  web = new UMWeb(Defaultcontent.url);  <br/>
     * web.setTitle("This is music title");//标题  <br/>
     * web.setThumb(thumb);  //缩略图  <br/>
     * web.setDescription("my description");//描述  <br/>
     *
     * @param video
     * @return
     */
    public UMShare shareVideo(UMVideo video)
    {
        shareAction.withMedia(video);
        return this;
    }

    /**
     * 分享音乐<br/>
     * 音乐只能使用网络音乐 <br/>
     * <p>
     * UMusic music = new UMusic(musicurl);//音乐的播放链接 <br/>
     * music.setTitle("This is music title");//音乐的标题 <br/>
     * music.setThumb("http://www.umeng.com/images/pic/social/chart_1.png");//音乐的缩略图 <br/>
     * music.setDescription("my description");//音乐的描述 <br/>
     * music.setmTargetUrl(Defaultcontent.url);//音乐的跳转链接 <br/>
     *
     * @param music
     * @return
     */
    public UMShare shareMusic(UMusic music)
    {
        shareAction.withMedia(music);
        return this;
    }

    /**
     * 分享Gif图片 <br/>
     * 目前只有微信好友分享支持Emoji表情，其他平台暂不支持 <br/>
     * UMEmoji emoji = new UMEmoji(this,"url"); <br/>
     * emoji.setThumb(new UMImage(this, R.drawable.thumb)); <br/>
     *
     * @param emoji
     * @return
     */
    public UMShare shareGif(UMEmoji emoji)
    {
        shareAction.withMedia(emoji);
        return this;
    }

    public void close()
    {
        shareAction.close();
    }

    public void open()
    {
        shareAction.open();
    }
}
